import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sbelov
 */
public class CrawlerStandard implements Crawler {
    private static final int TIMEOUT_VALUE = 10000;

    private static String readURL(URL page) throws IOException {
        URLConnection conn = page.openConnection();
        conn.setConnectTimeout(TIMEOUT_VALUE);
        conn.setReadTimeout(TIMEOUT_VALUE);
        return readString(conn.getInputStream());
    }

    private static String readString(InputStream stream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Crawler crawl(URL firstPage, Index index, Predicate<URL> filter, int maxPages, int depth) throws IOException {
    	if(maxPages==0){
    		return this;
    	}
    	
        String page = CrawlerStandard.readURL(firstPage);
        index.addPage(firstPage, page);
        
        String regex="\\b(?<=(href=\"))[^\"]*?(?=\")";
        Pattern pattern = Pattern.compile(regex);

        Matcher m = pattern.matcher(page);
        int count=0;
        while(m.find()) {
          System.out.println("URL: " + m.group());
          if(count==0) {
        	  count++;
        	  continue;
          }
          maxPages--;
          crawl(new URL(m.group()), index, filter, maxPages, depth);
        }
        
        return this;
    }
}
