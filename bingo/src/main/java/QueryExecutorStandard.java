import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import static java.util.Collections.emptySet;

/**
 * @author sbelov
 */
public class QueryExecutorStandard implements QueryExecutor {
    @Override
    public Collection<URL> query(String query, Index index) {
    	StringTokenizer tok = new StringTokenizer(query, " ");
    	Set<URL> all = new HashSet<URL>();
    	while(tok.hasMoreElements()) {
    		String word = tok.nextToken();
    		Collection<URL> set = index.getPagesWithWord(word);
    		all.addAll(set);
    	}
    	
    	return all;
    }
}
