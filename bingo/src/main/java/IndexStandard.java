import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import static java.util.Collections.emptySet;

/**
 * @author sbelov
 */
public class IndexStandard implements Index {
	private Map<String, Set<URL>>map = new HashMap<String, Set<URL>>();
	
    @Override
    public Index addPage(URL url, String content) {
    	StringTokenizer tok = new StringTokenizer(content, " ");
    	while(tok.hasMoreElements()) {
    		String word = tok.nextToken();
    		Set<URL> urls = map.get(word);
    		if(urls != null) {
    			urls.add(url);
    		} else {
    			Set<URL> set = new HashSet<URL>();
    			set.add(url);
    			map.put(word, set);
    		}
    	}
        return this;
    }

    @Override
    public Collection<URL> getPagesWithWord(String word) {
    	Set<URL> set = map.get(word);
        if(set != null) {
        	return set;
        }
        return emptySet();
    }
}
