import java.net.URL;
import java.util.Collection;

/**
 * Indexes pages by the words they contain
 */
public interface Index {

    /**
     * Adds a given page into the index
     * @param url page URL
     * @param content page content as as {@link java.lang.String}
     * @return instance of this {@link Index}
     */
    Index addPage(URL url, String content);

    /**
     * Retrieves all pages that contain a given word
     * @param word single word
     * @return a list of pages that contain a given word
     */
    Collection<URL> getPagesWithWord(String word);

}
