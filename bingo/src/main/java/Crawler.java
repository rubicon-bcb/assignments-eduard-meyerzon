import java.io.IOException;
import java.net.URL;

public interface Crawler {
    /**
     * Crawls web pages recursively, following the links (&lt;a href ...&gt; elements) encountered on each page
     * @param firstPage first page to add to the index and to follow the links
     * @param index index instance
     * @param maxPages upper bound on the number of pages crawled
     * @param filter URL filter (should return true for pages to be crawled and indexed)
     * @param depth levels of depth (1 = first page only, 2 = first page and pages it links to, ...)
     * @return an instance of this {@link Crawler}
     * @throws IOException
     */
    Crawler crawl(URL firstPage, Index index, Predicate<URL> filter, int maxPages, int depth) throws IOException;

}
