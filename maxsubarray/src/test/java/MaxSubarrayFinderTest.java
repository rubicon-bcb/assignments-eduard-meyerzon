import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class MaxSubarrayFinderTest {

    private MaxSubarrayFinder maxSubarrayFinder = new MaxSubarrayFinder();

    private static double[] ARRAY_ONE = new double[] {5, -1, 6, -5, 2, 1, -6, -5, 3, 5};
    private static double[] ARRAY_TWO = new double[] {-1, 1, 1, 2, -2, -1, 0, 0, -2, 1, 2, 0, -1, 2, -2, 2};
    private static double[] ARRAY_THREE = new double[] {-40, 8, -31, 14, -20, 46, -33, 44, 4, -2, -9};

    @Test
    public void testFindMaxSubarray() throws Exception {
        Subarray subarray = maxSubarrayFinder.findMaxSubarray(ARRAY_ONE);
        assertEquals(0, subarray.getFrom());
        assertEquals(2, subarray.getTo());
        assertSame(ARRAY_ONE, subarray.getOriginalArray());
        subarray = maxSubarrayFinder.findMaxSubarray(ARRAY_THREE);
        assertEquals(5, subarray.getFrom());
        assertEquals(8, subarray.getTo());
        assertSame(ARRAY_THREE, subarray.getOriginalArray());
    }

    @Test
    public void testFindMaxSubarraySum() throws Exception {
        assertEquals(10, maxSubarrayFinder.findMaxSubarraySum(ARRAY_ONE), 0);
        assertEquals(4, maxSubarrayFinder.findMaxSubarraySum(ARRAY_TWO), 0);
    }

}
